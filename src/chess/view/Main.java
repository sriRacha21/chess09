package chess.view;

import chess.controller.InputOutput;
import chess.model.Board;
import chess.model.Position;
import chess.model.Utility;
import chess.model.exceptions.IllegalMoveException;
import chess.model.pieces.King;
import chess.model.pieces.Piece;

import java.util.ArrayList;
import java.util.Arrays;

import static chess.controller.TestingSuite.*;

/**
 * The entry point for the Chess application.
 *
 * @author Noe Duran
 */
public class Main {

	/**
	 * Entry point method for chess application
	 * @param args Command line arguments
	 */
    public static void main(String[] args) {
//	    InputOutput loop = new InputOutput(getMiddlePawnsForwardTestingSuite());
//      InputOutput loop = new InputOutput(getDeadLiverOpening());
//	    InputOutput loop = new InputOutput(getScholarsMate());
//	    InputOutput loop = new InputOutput(getEnPassant());
//	    InputOutput loop = new InputOutput(getCastling());
//	    InputOutput loop = new InputOutput(getStandardCheck());
	    InputOutput loop = new InputOutput();
//      Board board = new Board(getKingsOnly());
//	    Board board = new Board(getKingsAndOneQueen());
//	    Board board = new Board(getEmptyBoard());
        Board board = new Board();
        loop.play(board);
    }

}
