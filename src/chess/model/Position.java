package chess.model;

import chess.model.exceptions.IllegalMoveException;
import chess.model.pieces.Piece;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Predicate;

import static chess.model.Utility.*;

/**
 * This class represents a location on the board.
 *
 * @author Arjun Srivastav
 */
public class Position {
	private int row;
	private int col;

	/**
	 * Specify a spot on the board with row and column indices.
	 * @param row 0-indexed row on the board.
	 * @param col 0-indexed column on the board.
	 */
	public Position(int row, int col) {
		if(row < 0 || row >= ROW_COUNT || col < 0 || col >= COLUMN_COUNT)
			throw new IllegalArgumentException("Illegal row or column for position");

		this.row = row;
		this.col = col;
	}

	/**
	 * Specify a spot on the board with a column letter and row number, e.g. "e4"
	 * @param pos String representing a spot on the board formatted as a column letter followed by a row number.
	 * @throws IllegalArgumentException If the passed string is not exactly 2 characters long.
	 */
	public Position(String pos) {
		if(pos.length() != 2)
			throw new IllegalArgumentException("Position isn't exactly 2 characters long.");

		char first = pos.charAt(0);
		if(!Arrays.asList('a','b','c','d','e','f','g','h').contains(first))
			throw new IllegalArgumentException("Column letter not valid.");
		this.col = charToColumnNumber(first);
		char second = pos.charAt(1);
		this.row = 8 - Integer.parseInt(second + "");
	}

	/**
	 * Get the positions between this position and another position
	 * @param other The other position.
	 * @return An array of positions between this position and the other position (exclusive).
	 */
	public ArrayList<Position> getBetween(Position other) {
		ArrayList<Position> betweenPos = new ArrayList<>();

		if(this.equals(other)) return betweenPos;

		if(getCol() == other.getCol()) {
			// vertical?
			if( getRow() < other.getRow() )
				for(int i = getRow() + 1; i < other.getRow(); i++ )
					betweenPos.add(new Position(i, getCol()));
			else if( other.getRow() < getRow() )
				for(int i = other.getRow() + 1; i < getRow(); i++ )
					betweenPos.add(new Position(i, getCol()));
		} else if(getRow() == other.getRow()) {
			// horizontal?
			if( getCol() < other.getCol() )
				for(int i = getCol() + 1; i < other.getCol(); i++ )
					betweenPos.add(new Position(getRow(), i));
			if( other.getCol() < getCol() )
				for(int i = other.getCol() + 1; i < getCol(); i++ )
					betweenPos.add(new Position(getRow(), i));
		} else if(Math.abs(getCol() - other.getCol()) == Math.abs(getRow() - other.getRow())) {
			// diagonal?
			int offset = Math.abs(getCol() - other.getCol());
			/*
					x
						o
			 */
			if(getRow() < other.getRow() && getCol() < other.getCol())
				for(int i = 1; i < offset; i++)
					betweenPos.add(new Position(getRow()+i, getCol()+i));
			/*
						x
					o
			 */
			if(getRow() < other.getRow() && getCol() > other.getCol())
				for(int i = 1; i < offset; i++)
					betweenPos.add(new Position(getRow()+i, getCol()-i));
			/*
					o
						x
			 */
			if(getRow() > other.getRow() && getCol() > other.getCol())
				for(int i = 1; i < offset; i++)
					betweenPos.add(new Position(getRow()-i, getCol()-i));
			/*
						o
					x
			 */
			if(getRow() > other.getRow() && getCol() < other.getCol())
				for(int i = 1; i < offset; i++)
					betweenPos.add(new Position(getRow()-i, getCol()+i));
		}

		return betweenPos;
	}

	/**
	 * Sometimes the worst possible implementation is the only one that makes sense. Check if this position is being attacked by a king.
	 * This method was made to protect from a stack overflow when two kings are too close.
	 * @param b The boyard to check for the attack b a king.
	 * @param fromColor The color that is checking for attacks from the opposite color.
	 * @return Whether or not this position is being attacked by the opposite color's king.
	 */
	public boolean isAttackedByKing(Board b, Color fromColor) {
		Position up;
		Position down;
		Position right;
		Position left;
		Position upRight;
		Position upLeft;
		Position downLeft;
		Position downRight;

		boolean attackedUp;
		boolean attackedDown;
		boolean attackedRight;
		boolean attackedLeft;
		boolean attackedUpRight;
		boolean attackedUpLeft;
		boolean attackedDownLeft;
		boolean attackedDownRight;

		try {
			up = new Position(getRow()-1, getCol());
			Piece pieceUp = b.getPiece(up);
			attackedUp = pieceUp != null && pieceUp.getType() == Type.KING && pieceUp.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedUp = false;
		}
		try {
			down = new Position(getRow()+1, getCol());
			Piece pieceDown = b.getPiece(down);
			attackedDown = pieceDown != null && pieceDown.getType() == Type.KING && pieceDown.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedDown = false;
		}
		try {
			right = new Position(getRow(), getCol()+1);
			Piece pieceRight = b.getPiece(right);
			attackedRight = pieceRight != null && pieceRight.getType() == Type.KING && pieceRight.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedRight = false;
		}
		try {
			left = new Position(getRow(), getCol()-1);
			Piece pieceLeft = b.getPiece(left);
			attackedLeft = pieceLeft != null && pieceLeft.getType() == Type.KING && pieceLeft.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedLeft = false;
		}
		try {
			upRight = new Position(getRow()-1, getCol()+1);
			Piece pieceUpRight = b.getPiece(upRight);
			attackedUpRight = pieceUpRight != null && pieceUpRight.getType() == Type.KING && pieceUpRight.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedUpRight = false;
		}
		try {
			upLeft = new Position(getRow()-1, getCol()-1);
			Piece pieceUpLeft = b.getPiece(upLeft);
			attackedUpLeft = pieceUpLeft != null && pieceUpLeft.getType() == Type.KING && pieceUpLeft.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedUpLeft = false;
		}
		try {
			downLeft = new Position(getRow()+1, getCol()-1);
			Piece pieceDownLeft = b.getPiece(downLeft);
			attackedDownLeft = pieceDownLeft != null && pieceDownLeft.getType() == Type.KING && pieceDownLeft.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedDownLeft = false;
		}
		try {
			downRight = new Position(getRow() + 1, getCol() + 1);
			Piece pieceDownRight = b.getPiece(downRight);
			attackedDownRight = pieceDownRight != null && pieceDownRight.getType() == Type.KING && pieceDownRight.getColor() != fromColor;
		} catch( IllegalArgumentException iae ) {
			attackedDownRight = false;
		}

		return attackedUp || attackedDown || attackedRight || attackedLeft || attackedUpRight || attackedUpLeft || attackedDownLeft || attackedDownRight;
	}

	/**
	 * Check if this position is being attacked
	 * @param b The instance of the board to check the position's status for.
	 * @param fromColor The color that is checking if the opposite color is attacking this position.
	 * @return Whether or not this piece is being attacked by the opposite color (not fromColor).
	 */
	public boolean isAttacked(Board b, Color fromColor) {
		for(int i = 0; i < ROW_COUNT; i++) {
			for(int j = 0; j < COLUMN_COUNT; j++) {
				Position pos = new Position(i,  j);
				Piece piece = b.getPiece(pos);
				// if theres a piece at that position
				if(piece == null)
					continue;
				// and its color is not yours
				if(piece.getColor() == fromColor)
					continue;
				// constructing move object
				Move move;
				try {
					move = new Move(pos, this, getOppositeColor(fromColor), false);
				} catch(IllegalMoveException ime) {
					continue;
				}
				// remove the piece there and replace it
				Piece oldPiece = b.getPiece(this);
				b.setPiece(this, null);
				// and the that piece can move to this position, then it's being attacked
				if(piece.checkMove(move, b)) {
					b.setPiece(this, oldPiece);
					return true;
				}
				b.setPiece(this, oldPiece);
			}
		}

		return false;
	}

	/**
	 * Get the position above this one.
	 * @return Position above.
	 */
	public Position up() {
		return getRow() == 0 ? null : new Position(getRow()-1, getCol());
	}

	/**
	 * Get the position below this one.
	 * @return Position below.
	 */
	public Position down() {
		return getRow() == ROW_COUNT-1 ? null : new Position(getRow()+1, getCol());
	}

	/**
	 * Get the position to the left of this one.
	 * @return Position left.
	 */
	public Position left() {
		return getCol() == 0 ? null : new Position(getRow(), getCol()-1);
	}

	/**
	 * Get the position to the right of this one.
	 * @return Position right.
	 */
	public Position right() {
		return getCol() == COLUMN_COUNT-1 ? null : new Position(getRow(), getCol()+1);
	}

	/**
	 * Get the position to the top-left of this one.
	 * @return Top-left position.
	 */
	public Position upLeft() {
		return getRow() == 0 || getCol() == 0 ? null : new Position(getRow()-1, getCol()-1);
	}

	/**
	 * Get the position to the top-right of this one.
	 * @return Top-right position.
	 */
	public Position upRight() {
		return getRow() == 0 || getCol() == COLUMN_COUNT-1 ? null : new Position(getRow()-1, getCol()+1);
	}

	/**
	 * Get the position to the bottom-left of this one.
	 * @return Bottom-left position.
	 */
	public Position downLeft() {
		return getRow() == ROW_COUNT-1 || getCol() == 0 ? null : new Position(getRow()+1, getCol()-1);
	}

	/**
	 * Get the position bottom-right of this one.
	 * @return Bottom-right position.
	 */
	public Position downRight() {
		return getRow() == ROW_COUNT-1 || getCol() == COLUMN_COUNT-1 ? null : new Position(getRow()+1, getCol()+1);
	}

	/**
	 * Getter for row field.
	 * @return row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Getter for column field.
	 * @return column
	 */
	public int getCol() {
		return col;
	}

	/**
	 * Compares row and column to determine if two positions are equal.
	 * @param other The other object to determine equality.
	 * @return Whether or not this object and the other object are equal.
	 */
	@Override
	public boolean equals(Object other) {
		if(other == this) return true;
		if(!(other instanceof Position))
			return false;
		Position o = (Position) other;
		return getRow() == o.getRow() && getCol() == o.getCol();
	}

	/**
	 * Get the "letter-number" representation for this position.
	 * @return "letter-number" representation for this position.
	 */
	@Override
	public String toString() {
		char letter = intToColumnLetter(getCol());
		int row = 8 - getRow();
		return "" + letter + row;
	}

	/**
	 * Get the "row-column" representation for this position.
	 * @return "row-column" representation for this position.
	 */
	public String toStringRowCol(){
		return "(" + row + ", " + col + ")";
	}

	/**
	 * Quality-of-life toString that also finds the piece color and type at this position.
	 * @param b The board to check the position for.
	 * @return The "letter-number" representation for this position with the piece and piece type concatenated.
	 */
	public String toString(Board b){
		return toString()
				+ " ["
				+ (b.getPiece(this) == null
					? ""
					: colorToStringHR(b.getPiece(this).getColor()) + " ")
				+ (b.getPiece(this) == null
					? "No Piece Found"
					: typeToStringHR(b.getPiece(this).getType())
				) + "]";
	}
}
