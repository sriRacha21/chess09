package chess.model;

import chess.model.Utility.Color;
import chess.model.exceptions.IllegalMoveException;
import chess.model.pieces.Piece;
import chess.model.pieces.*;

import java.util.ArrayList;

import static chess.model.Utility.*;

/**
 * This classes models the chess board.
 *
 * @author Arjun Srivastav
 */
public class Board {
	private Piece[][] board;
	private Piece prevPiece;


	/**
	 * Instantiate a default board with the default layout.
	 */
	public Board() {
		board = new Piece[ROW_COUNT][COLUMN_COUNT];

		for(int i = 0; i < ROW_COUNT; i++)
			for(int j = 0; j < COLUMN_COUNT; j++)
				board[i][j] = null;

		// place all pawns
		for(int j = 0; j < ROW_COUNT; j++) {
			board[ROW_COUNT - 2][j] = new Pawn(Color.WHITE);
			board[1][j] = new Pawn(Color.BLACK);
		}
		// place rooks
		board[0][0] = new Rook(Color.BLACK);
		board[ROW_COUNT - 1][0] = new Rook(Color.WHITE);
		board[0][7] = new Rook(Color.BLACK);
		board[ROW_COUNT - 1][7] = new Rook(Color.WHITE);
		// place knights
		board[0][1] = new Knight(Color.BLACK);
		board[ROW_COUNT - 1][1] = new Knight(Color.WHITE);
		board[0][6] = new Knight(Color.BLACK);
		board[ROW_COUNT - 1][6] = new Knight(Color.WHITE);
		// place bishops
		board[0][2] = new Bishop(Color.BLACK);
		board[ROW_COUNT - 1][2] = new Bishop(Color.WHITE);
		board[0][5] = new Bishop(Color.BLACK);
		board[ROW_COUNT - 1][5] = new Bishop(Color.WHITE);
		// place queen
		board[0][3] = new Queen(Color.BLACK);
		board[ROW_COUNT - 1][3] = new Queen(Color.WHITE);
		// place king
		board[0][4] = new King(Color.BLACK);
		board[ROW_COUNT - 1][4] = new King(Color.WHITE);
	}

	/**
	 * Instantiate a board with a given layout
	 * @param board The board to use to generate the instance of a Board object.
	 */
	public Board(Piece[][] board) {
		this.board = board;
	}

	@Deprecated
	public Piece[][] getBoard() { return board; }

	/**
	 * Get the piece located at a certain position.
	 * @param pos The piece to get at the position.
	 * @return The piece at that position (null if there is no piece).
	 */
	public Piece getPiece(Position pos) { return board[pos.getRow()][pos.getCol()]; }

	public Piece getPrevPiece() { return this.prevPiece; }

	/**
	 * Set the piece at a certain position to a certain piece.
	 * @param pos The position on the board whose piece needs to be set.
	 * @param piece The piece to place at that position.
	 */
	public void setPiece(Position pos, Piece piece) { board[pos.getRow()][pos.getCol()] = piece; }
	public Position findPiece(Piece p) {
		for(int i = 0; i < ROW_COUNT; i++)
			for(int j = 0; j < COLUMN_COUNT; j++)
				if(p.equals(getPiece(new Position(i,j))))
					return new Position(i,j);
		return null;
	}

	/**
	 * Attempt to move a piece on the board.
	 * @param move The move that is being attempted to be made.
	 * @throws IllegalMoveException Thrown if there is no piece located at that position or if the move is illegal.
	 */
	public void move(Move move) throws IllegalMoveException {
		Position from = move.getFrom();
		Position to = move.getTo();

		// find the piece we're moving
		Piece piece = getPiece(from);

		if(piece == null)
			throw new IllegalMoveException("There is no piece located at this position.");

		if( DEBUG )
			System.out.println("Attempting to move from " + from.toString(this) + " to " + to.toString(this));

		// check if the move is legal
		boolean isLegalMove = piece.checkMove(move, this);
		if( !isLegalMove )
			throw new IllegalMoveException("Piece cannot be moved to that position: " + to.toString(this));

		//CASTLING
		if(piece.getType() == Type.KING && piece.isCastling(move,this)){
			//If right rook castling
			if(getPiece(new Position(to.getRow(), to.getCol()+1)).getType() == Type.ROOK){
				//Moving Knight
				setPiece(to, getPiece(from));
				setPiece(from, null);
				piece.setMoved(true);

				//Moving Rook
				setPiece(new Position(to.getRow(), to.getCol()-1), getPiece(new Position(to.getRow(), to.getCol()+1)));
				setPiece(new Position(to.getRow() , to.getCol()+1), null);
			}

			//if Left rook castling
			else if(getPiece(new Position(to.getRow(), to.getCol()-2)).getType() == Type.ROOK){
				//Moving Knight
				setPiece(to, getPiece(from));
				setPiece(from, null);
				piece.setMoved(true);

				//Moving Rook
				Piece rook =  getPiece(new Position(to.getRow(), to.getCol()-2));
				setPiece(new Position(to.getRow(), to.getCol()-2),rook);
				setPiece(new Position(to.getRow() , to.getCol()+3), null);
				rook.setMoved(true);
			}
		}

		//PROMOTION
		else if(piece.getType() == Type.PAWN && (to.getRow() == 7 || to.getRow() == 0)){

			piece.setType(piece.stringToType(move.getPromotionTo()));

			setPiece(to, getPiece(from));
			setPiece(from, null);

			piece.setMoved(true);
		}

		//En Passant
		//Doesnt check if first move for capture piece
		else if(piece.isEnPassant(move,this, prevPiece)){
			int pieceColor = piece.getColor() == Color.BLACK ? -1 : 1;
			setPiece(to, getPiece(from));
			setPiece(from, null);
			piece.setMoved(true);

			setPiece(new Position(to.getRow() + pieceColor, to.getCol()), null);
		}
		else{
			setPiece(to, getPiece(from));
			setPiece(from, null);
			piece.setMoved(true);
		}

		// ending check
		if(!move.doesEndCheck(this))
			throw new IllegalMoveException("Move does not end check.");

		//Keeps check of the previous piece
		prevPiece = piece;
	}

	/**
	 * Finds out if there are pieces between from and to (exclusive).
	 * @param from The position started from.
	 * @param to The position travelling to.
	 * @return Whether or not pieces exist between from and to.
	 */
	public boolean isPiecesBetween(Position from, Position to) {
		ArrayList<Position> betweenPositions = from.getBetween(to);

		for(Position p : betweenPositions)
			if(getPiece(p) != null)
				return true;
		return false;
	}

	/**
	 * An ASCII-ish picture of the board. This is used for printing the board between turns.
	 * @return An ASCII-ish picture of the board.
	 */
	@Override
	public String toString() {
		StringBuilder outStr = new StringBuilder();
		boolean isCheckered = false;
		for(int i = 0; i < ROW_COUNT; i++) {
			for (int j = 0; j < COLUMN_COUNT; j++) {
				Piece p = getPiece(new Position(i,j));
				outStr.append(p == null ? (isCheckered ? "##" : "  ") : p.toString()).append(" ");
				isCheckered = !isCheckered;
			}
			isCheckered = !isCheckered;
			// row numbers
			outStr.append(8-i).append("\n");
		}
		// column letters
		outStr.append(" ");
		for(int i = 0; i < ROW_COUNT; i++)
			outStr.append(Utility.intToColumnLetter(i)).append("  ");

		return "\n" + outStr.toString() + "\n";
	}
}
