package chess.model;

/**
 * Utility class for Colors, Types, PlayStates, other enums utilities that may need to be shared between many classes.
 */
public abstract class Utility {
	/**
	 * Whether or not the program is in debugging mode.
	 */
	public static final boolean DEBUG = false;

	/**
	 * The number of rows on a chess board.
	 */
	public static final int ROW_COUNT = 8;
	/**
	 * The number of columns on a chess board.
	 */
	public static final int COLUMN_COUNT = 8;

	/**
	 * The possible colors of the sides.
	 */
	public enum Color {
		BLACK,
		WHITE
	}

	/**
	 * The possible types a chess piece can be.
	 */
	public enum Type {
		PAWN,
		ROOK,
		KNIGHT,
		BISHOP,
		QUEEN,
		KING,
	}

	/**
	 * The possible play states of the board.
	 */
	public enum PlayState {
		NORMAL,
		TRY_AGAIN,
		RESIGNATION,
		DRAW,
		CHECKMATE,
		STALEMATE,
		CHECK
	}

	/**
	 * Utility method to turn a color enum to a human-readable string.
	 * @param c The color to convert to a string.
	 * @return The color converted to a string.
	 */
	public static String colorToStringHR(Color c) {
		if(c == Color.BLACK) return "Black";
		else if(c == Color.WHITE) return "White";
		else throw new IllegalArgumentException("Invalid color!");
	}

	/**
	 * Utility method to turn a type enum to a human-readable string.
	 * @param t The type to convert to a string.
	 * @return The type converted to a string.
	 */
	public static String typeToStringHR(Type t) {
		if(t == Type.PAWN) return "Pawn";
		else if(t == Type.ROOK) return "Rook";
		else if(t == Type.KNIGHT) return "Knight";
		else if(t == Type.BISHOP) return "Bishop";
		else if(t == Type.QUEEN) return "Queen";
		else if(t == Type.KING) return "King";
		else throw new IllegalArgumentException("Invalid type!");
	}

	/**
	 * Utility method to get the opposing color.
	 * @param c The color to get the opposing color of.
	 * @return The opposing color of the given color.
	 */
	public static Color getOppositeColor(Color c) {
		if(c == Color.BLACK) return Color.WHITE;
		else if(c == Color.WHITE) return Color.BLACK;
		else throw new IllegalArgumentException("Invalid color!");
	}

	/**
	 * Utility method to turn an integer (0-indexed) into a column letter.
	 * @param col_number The column number (0-indexed) to turn into a column letter.
	 * @return The letter associated with that column number.
	 */
	public static char intToColumnLetter(int col_number) {
		return switch (col_number) {
			case 0 -> 'a';
			case 1 -> 'b';
			case 2 -> 'c';
			case 3 -> 'd';
			case 4 -> 'e';
			case 5 -> 'f';
			case 6 -> 'g';
			case 7 -> 'h';
			default -> throw new IllegalStateException("Unexpected value: " + col_number);
		};
	}

	/**
	 * Utility method to turn a character into a column number (0-indexed).
	 * @param row_letter The character to turn into a column number (0-indexed).
	 * @return The associated column number with the column letter.
	 */
	public static int charToColumnNumber(char row_letter) {
		return switch (row_letter) {
			case 'a' -> 0;
			case 'b' -> 1;
			case 'c' -> 2;
			case 'd' -> 3;
			case 'e' -> 4;
			case 'f' -> 5;
			case 'g' -> 6;
			case 'h' -> 7;
			default -> throw new IllegalStateException("Unexpected value: " + row_letter);
		};
	}
}
