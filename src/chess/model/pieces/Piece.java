package chess.model.pieces;

import chess.model.Board;
import chess.model.Move;
import chess.model.Position;
import chess.model.Utility.Color;
import chess.model.Utility.Type;

import java.lang.IllegalStateException;

/**
 * This abstract class models a chess piece. It cannot be instantiated because every piece on the board must be of some type.
 *
 * @author Arjun Srivastav
 */
public abstract class Piece {

	private final Color color;
	private  Type type;

	private boolean moved;

	/**
	 * Check if the piece has ever been moved.
	 * @return Whether the piece has been moved.
	 */
	public boolean isMoved() {
		return moved;
	}

	/**
	 * Set whether the piece has been moved.
	 * @param moved Whether or not the piece was just moved.
	 */
	public void setMoved(boolean moved) {
		this.moved = moved;
	}

	/**
	 * Sets the type of the piece to the new piece.
	 * @param type The new type of the piece.
	 */
	public void setType(Type type) {
		this.type = type;
	}



	/**
	 * Construct a piece object. This is intended to only be used for super() calls.
	 *
	 * @param color The color of this piece.
	 * @param type The type of this piece (King, Queen, Knight, etc.)
	 */
	public Piece(Color color, Type type) {
		this.color = color;
		this.type = type;
		this.moved = false;
	}

	/**
	 * Check if a move is valid for this piece.
	 * @param move The move to check the validity of.
	 * @param board The board to check the validity of said move for said piece on.
	 * @return Whether or not this move is valid for this piece. Since this is a generic piece, if this returns false,
	 * this move will be invalid for all pieces.
	 */
	public boolean checkMove(Move move, Board board) {

		Position from = move.getFrom();
		Position to = move.getTo();

		// find the piece we're moving
		Piece piece = board.getPiece(from);

		// check if the piece is moving to the same position
		if( from.equals(to) ){
			return false;}
		// check if someone is trying to move their opponents color
		if( move.getMovingColor() != piece.getColor() ){
			return false;}
		// check if there are pieces in-between
		if( getType() != Type.KNIGHT && board.isPiecesBetween(from, to) ){
			return false;}
		// check if you are landing on a piece of your own color
		if( board.getPiece(to) != null && move.getMovingColor() == board.getPiece(to).getColor() ){
			return false;}

		return true;
	};
	public boolean isCastling(Move move, Board board) {
		Position from = move.getFrom();
		Position to = move.getTo();

		//Checking for castling
		if(Math.abs(from.getCol()-to.getCol()) <= 1 && Math.abs(from.getRow()-to.getRow()) <= 1) {
			return false;
		}

		return true;
	};


	/**
	 * Get the color of this piece.
	 * @return The color of this piece.
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * Get the type of this piece.
	 * @return The type of this piece.
	 */
	public Type getType() {
		return type;
	}

	/**
	 * Get the position of this piece on the board.
	 * @param b The board to find the piece on
	 * @return The position of this piece on the board.
	 */
	public Position getPosition(Board b) {
		return b.findPiece(this);
	}

	/**
	 * Utility method to convert the piece's color to a string.
	 * @return String representing the piece's color.
	 */
	public String colorToString() {
		if(color == Color.BLACK) return "b";
		if(color == Color.WHITE) return "w";
		else throw new IllegalStateException();
	}

	/**
	 * Utility method to conver the piece's type to a string.
	 * @return String representing the piece's type.
	 */
	public String typeToString() {
		if(type == Type.PAWN) return "p";
		if(type == Type.ROOK) return "R";
		if(type == Type.KNIGHT) return "N";
		if(type == Type.BISHOP) return "B";
		if(type == Type.QUEEN) return "Q";
		if(type == Type.KING) return "K";
		else throw new IllegalStateException();
	}

	/**
	 * Utility method to convert the string to a piece's type.
	 * @param string The string to turn into a piece type
	 * @return String representing the piece's type.
	 */
	public Type stringToType(String string) {
		if(string.isEmpty()) return Type.QUEEN;
		if(string.equalsIgnoreCase("P")) return Type.PAWN;
		if(string.equalsIgnoreCase("R")) return Type.ROOK;
		if(string.equalsIgnoreCase("N")) return Type.KNIGHT;
		if(string.equalsIgnoreCase("B")) return Type.BISHOP;
		if(string.equalsIgnoreCase("Q")) return Type.QUEEN;
		if(string.equalsIgnoreCase("K")) throw new IllegalStateException("Can't change pawn to king!");
		else throw new IllegalStateException();
	}

	/**
	 * Check if this piece is the same as another piece
	 * @param o The other object to check equality for.
	 * @return Whether this piece is the same as the other object.
	 */
	@Override
	public boolean equals(Object o) {
		if(o== this) return true;
		if(!(o instanceof Piece))
			return false;
		Piece other = (Piece) o;
		return other.getType() == getType() && other.getColor() == getColor();
	}

	/**
	 * Get the piece as the color and the type.
	 * @return A string representing the piece.
	 */
	@Override
	public String toString() {
		return colorToString() + typeToString();
	}

	public boolean isEnPassant(Move move, Board board, Piece prevPiece) {
		return false;
	}
}
