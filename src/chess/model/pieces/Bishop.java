package chess.model.pieces;

import chess.model.Board;
import chess.model.Move;
import chess.model.Utility;

/**
 * This class represents a Bishop.
 *
 * @author Arjun Srivastav
 */
public class Bishop extends Piece {

    public Bishop(Utility.Color color) {
        super(color, Utility.Type.BISHOP);
    }

    public static boolean checkMove(Move move, Piece piece, Board board) {
        return false;
    }

    /**
     * Check if the Bishop can make that move.
     * @param move The move to check the validity of.
     * @param board The board to check the validity of said move for said piece on.
     * @return Whether or not the bishop can make that move.
     */
    @Override
    public boolean checkMove(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;

        int fromCol = move.getFrom().getCol();
        int fromRow = move.getFrom().getRow();
        int toCol   = move.getTo().getCol();
        int toRow   = move.getTo().getRow();

    	if(Math.abs(fromCol - toCol) == Math.abs(fromRow - toRow))
    	    return true;

    	return false;
    }
}
