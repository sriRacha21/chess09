package chess.model.pieces;

import chess.model.Board;
import chess.model.Move;
import chess.model.Position;
import chess.model.Utility;

import static chess.model.Utility.ROW_COUNT;

/**
 * This class represents a Rook.
 *
 * @author Arjun Srivastav
 */
public class Rook extends Piece {
    /**
     * Construct a Rook.
     * @param color The color of the rook.
     */
    public Rook(Utility.Color color) {
        super(color, Utility.Type.ROOK);
    }

    /**
     * Check whether the rook can make that move.
     * @param move The move to check the validity of.
     * @param board The board to check the validity of said move for said piece on.
     * @return Whether the Rook can make the move.
     */
    @Override
    public boolean checkMove(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;

        int fromCol = move.getFrom().getCol();
        int fromRow = move.getFrom().getRow();
        int toCol   = move.getTo().getCol();
        int toRow   = move.getTo().getRow();

	    if( toRow == fromRow || toCol == fromCol )
            return true;
        return false;
    }
}
