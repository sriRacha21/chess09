package chess.model.pieces;

import chess.model.Board;
import chess.model.Move;
import chess.model.Utility;

/**
 * This class represents a Queen piece.
 *
 * @author Arjun Srivastav
 */
public class Queen extends Piece {
    public Queen(Utility.Color color) {
        super(color, Utility.Type.QUEEN);
    }

    /**
     * Check if the queen can move to the position. Simply checks if a rook or bishop can move there.
     * @param move The move to check the validity of.
     * @param board The board to check the validity of said move for said piece on.
     * @return Whether the piece can make the move.
     */
    @Override
    public boolean checkMove(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;

    	// if it's a valid rook or bishop move it's fine
        Utility.Color surrogateColor = move.getMovingColor();

        return new Rook(surrogateColor).checkMove(move, board) || new Bishop(surrogateColor).checkMove(move, board);
    }
}
