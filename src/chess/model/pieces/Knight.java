package chess.model.pieces;

import chess.model.Board;
import chess.model.Move;
import chess.model.Utility;

/**
 * This class represents a Knight.
 *
 * @author Arjun Srivastav
 */
public class Knight extends Piece {
    public Knight(Utility.Color color) {
        super(color, Utility.Type.KNIGHT);
    }

    /**
     * Check if the knight can make that move.
     * @param move The move to check the validity of.
     * @param board The board to check the validity of said move for said piece on.
     * @return Whether or not the Knight can make the move.
     */
    @Override
    public boolean checkMove(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;

        int fromCol = move.getFrom().getCol();
        int fromRow = move.getFrom().getRow();
        int toCol   = move.getTo().getCol();
        int toRow   = move.getTo().getRow();

        if(Math.abs(fromRow-toRow) == 2 && Math.abs(fromCol-toCol) == 1)
            return true;
        else if(Math.abs(fromRow-toRow) == 1 && Math.abs(fromCol-toCol) == 2)
        	return true;

        return false;
    }
}
