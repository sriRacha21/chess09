package chess.model.pieces;

import chess.model.Board;
import chess.model.Move;
import chess.model.Position;
import chess.model.Utility;

import java.util.ArrayList;

/**
 * This class represents a King piece.
 *
 * @author Arjun Srivastav
 */
public class King extends Piece {
    /**
     * Create a King of a certain color.
     * @param color The color to make this King.
     */
    public King(Utility.Color color) {
        super(color, Utility.Type.KING);
    }


    /**
     * Check if the King can make this move.
     * @param move The move to check the validity of.
     * @param board The board to check the validity of said move for said piece on.
     * @return Whether or not this move can be made.
     */
    @Override
    public boolean checkMove(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;

        int fromCol = move.getFrom().getCol();
        int fromRow = move.getFrom().getRow();
        int toCol   = move.getTo().getCol();
        int toRow   = move.getTo().getRow();

        // let the king move one space in any direction
        if(Math.abs(fromCol-toCol) <= 1 && Math.abs(fromRow-toRow) <= 1) {
            // check if the space the king is moving to is being attacked (for performance reasons, this is checked only
            // if the king is being moved to a space it can possibly move to.

//            if(move.getTo().isAttackedByKing(board, getColor()) || move.getTo().isAttacked(board, getColor()))
//                return false;
            return true;
        }


        //Checking for castling
        if(Math.abs(fromCol-toCol) <= 2 && Math.abs(fromRow-toRow) <= 2 && !isMoved()) {
            return true;
        }

        return false;
    }

    /**
     * Get all the legal new positions of the king after a move.
     * @param board The board with which to check where the king can move.
     * @return All the positions the King can move to.
     */
    public ArrayList<Position> getLegalPositions(Board board) {
        ArrayList<Position> legalPositions = new ArrayList<>();
        Position thisPos = getPosition(board);

        legalPositions.add(thisPos.up());
        legalPositions.add(thisPos.left());
        legalPositions.add(thisPos.right());
        legalPositions.add(thisPos.down());
        legalPositions.add(thisPos.upLeft());
        legalPositions.add(thisPos.upRight());
        legalPositions.add(thisPos.downLeft());
        legalPositions.add(thisPos.downRight());

        ArrayList<Position> newLegalPositions = new ArrayList<>();

        for(Position legalPosition : legalPositions)
            if(legalPosition != null)
                newLegalPositions.add(legalPosition);

        return newLegalPositions;
    }

    public boolean checkCastling(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;

        int fromCol = move.getFrom().getCol();
        int fromRow = move.getFrom().getRow();
        int toCol   = move.getTo().getCol();
        int toRow   = move.getTo().getRow();

        // let the king move one space in any direction
        if(Math.abs(fromCol-toCol) <= 1 && Math.abs(fromRow-toRow) <= 1) {
            // check if the space the king is moving to is being attacked (for performance reasons, this is checked only
            // if the king is being moved to a space it can possibly move to.
            if(move.getTo().isAttackedByKing(board, getColor()) || move.getTo().isAttacked(board, getColor()))
                return false;
        }


        //Checking for castling
        if(Math.abs(fromCol-toCol) <= 2 && Math.abs(fromRow-toRow) <= 2 && !isMoved()) {
            return true;
        }

        return false;
    }
}
