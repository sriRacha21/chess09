package chess.model.pieces;

import chess.model.Board;
import chess.model.Utility;
import chess.model.Position;
import chess.model.Move;

/**
 * This class represents a Pawn.
 *
 * @author Arjun Srivastav
 */
public class Pawn extends Piece {
    public Pawn(Utility.Color color) {
        super(color, Utility.Type.PAWN);
    }

    /**
     *
     * @param move The move to check the validity of
     * @param board The board to check the validity of said move for said piece on.
     * @param prevPiece The previous moved piece to check if its a pawn for valid En Passant
     * @return Whether or not the move is EnPassant and valid
     */

    @Override
    public boolean isEnPassant(Move move, Board board, Piece prevPiece){
        if(prevPiece != null){
            if(prevPiece.getType() == Utility.Type.PAWN) {
                int pieceColor = getColor() == Utility.Color.BLACK ? -1 : 1;
                //Piece that will be captured from en passant movement
                Piece capturePiece = board.getPiece(new Position(move.getTo().getRow() + pieceColor, move.getTo().getCol()));
                //Checks if the pawn that will be captured exist
                if (capturePiece != null ) {
                    if (capturePiece.getType() == Utility.Type.PAWN) {
                        return true;
                    }
                }
                return false;
            }
        }
        return false;
    }
    /**
     * Check if the pawn can make a move.
     * @param move The move to check the validity of.
     * @param board The board to check the validity of said move for said piece on.
     * @return Whether or not the pawn can make that move.
     */
    @Override
    public boolean checkMove(Move move, Board board) {
        if(!super.checkMove(move, board))
            return false;


        int pieceColor = getColor() == Utility.Color.BLACK ? -1 : 1;

        int fromCol = move.getFrom().getCol();
        int fromRow = move.getFrom().getRow();
        int toCol = move.getTo().getCol();
        int toRow = move.getTo().getRow();

        Piece capturedPiece = board.getPiece(move.getTo());

        // movement rules
        if ((fromRow - toRow) == (2 * pieceColor) && toCol == fromCol && !isMoved() && capturedPiece == null) {
            // double space forward
            return true;
        } else if ((fromRow - toRow) == pieceColor && toCol == fromCol && capturedPiece == null) {
            // single space forward
            return true;
        }


        if(Math.abs(fromCol - toCol) == Math.abs(fromRow - toRow))
            return true;

        // capturing rules
        if (capturedPiece != null
                && fromRow - toRow == pieceColor
                && Math.abs(fromCol - toCol) == 1
                && capturedPiece.getColor() != move.getMovingColor())
            return true;

        return false;
    }
}
