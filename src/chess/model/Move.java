package chess.model;

import chess.model.exceptions.IllegalMoveException;
import chess.model.Utility.Color;
import chess.model.pieces.King;
import chess.model.pieces.Piece;
import chess.model.Board;

import static chess.model.Utility.getOppositeColor;

/**
 * This class represents a move made.
 *
 * @author Arjun Srivastav
 */
public class Move {
	/**
	 * The position the piece is moving from.
	 */
	private Position from;
	/**
	 * The position the piece is moving to.
	 */
	private Position to;
	/**
	 * The color of the side that is making the move.
	 */
	private Color movingColor;
	/**
	 * Whether or not the player is requesting a draw at that time.
	 */
	private boolean drawRequest;
	/**
	 * The piece the player wants to promote the pawn to.
	 */
	private String promotionTo;


	/**
	 * Whether the player performing this move is in check.
	 */
	private boolean inCheck;

	/**
	 * Construct a move object.
	 * @param from The position the piece is being moved from.
	 * @param to The position the piece is being moved to.
	 * @param movingColor The color of the side making the move.
	 * @param drawRequest Whether or not the player is requesting a draw at that time.
	 * @param inCheck Whether the player performing this move is in check.
	 * @throws IllegalMoveException If from or to are null.
	 */
	public Move(Position from, Position to, Color movingColor, boolean drawRequest, boolean inCheck) throws IllegalMoveException {
		if(from == null || to == null) throw new IllegalMoveException("One or more null arguments.");

		// assume move is legal
		this.from = from;
		this.to = to;
		this.movingColor = movingColor;
		this.drawRequest = drawRequest;
		this.inCheck = inCheck;
	}

	/**
	 * Backwards compatibility constructor with a default not in check state.
	 * @param from The position the piece is being moved from.
	 * @param to The position the piece is being moved to.
	 * @param movingColor The color of the side making the move.
	 * @param drawRequest Whether or not the player is requesting a draw at that time.
	 * @throws IllegalMoveException If from or to are null.
	 */
	public Move(Position from, Position to, Color movingColor, boolean drawRequest) throws IllegalMoveException {
		this(from, to, movingColor, drawRequest, false);
	}

	/**
	 * Process a move as a string before constructing a move object. Simply converts user input to the necessary object.
	 * @param toProcess The string to process. Generally user input.
	 * @param movingColor The color of the side making the move.
	 */
	//user input as string, process string and call other constructor
	public Move(String toProcess, Color movingColor) {
		String[] input = toProcess.split(" ");
		if(input.length < 2 || input.length > 3) {
			throw new IllegalArgumentException("Illegal input");
		}

		Position fromCoord = new Position(input[0]);
		Position toCoord = new Position(input[1]);

		// assume move is legal
		this.from = fromCoord;
		this.to = toCoord;
		this.movingColor = movingColor;
		this.drawRequest = input.length == 3 && input[2].equalsIgnoreCase("draw?");

		if(input.length == 3){
			//Not empty and not draw
			if(!input[2].isEmpty()  && !input[2].equalsIgnoreCase("draw?")) {
				this.promotionTo = input[2];
			}else this.promotionTo = "Q";
		}else this.promotionTo = "Q";

	}

	public boolean doesEndCheck(Board b) throws IllegalMoveException {
		// if the player is not even in check the move is allowed
		if(!isInCheck()) return true;
		// if the player is in check, check if this move will end check
		b.move(this);
		// check if the moving color is still in check
		Position kingPos = b.findPiece(new King(movingColor));
		if(kingPos.isAttacked(b, getOppositeColor(movingColor))) {
			b.move(new Move(to, from, getMovingColor(), isDrawRequest()));
			return false;
		} else {
			b.move(new Move(to, from, getMovingColor(), isDrawRequest()));
			return true;
		}
	}

	/**
	 * Getter for the from field.
	 * @return from position
	 */
	public Position getFrom() {
		return from;
	}

	/**
	 * Getter for the to field.
	 * @return to position
	 */
	public Position getTo() {
		return to;
	}

	/**
	 * Getter for the moving color field.
	 * @return The color of the side that is making the move.
	 */
	public Color getMovingColor() {
		return movingColor;
	}

	public String getPromotionTo() {
		return promotionTo;
	}

	/**
	 * Getter for the drawRequest field.
	 * @return Whether or not the player is requesting a draw in that move.
	 */
	public boolean isDrawRequest() {
		return drawRequest;
	}

	/**
	 * Getter for the inCheck field.
	 * @return Whether or not the player is in check for that move.
	 */
	public boolean isInCheck() {
		return inCheck;
	}
}
