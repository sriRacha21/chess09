package chess.model.exceptions;

/**
 * Custom exception for illegal moves.
 */
public class IllegalMoveException extends Exception {
	/**
	 * Construct an IllegalMoveException. Message is mandatory.
	 * @param message The reason why the move cannot be made.
	 */
	public IllegalMoveException(String message) {
		super(message);
	}
}