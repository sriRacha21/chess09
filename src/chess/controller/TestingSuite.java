package chess.controller;

import chess.model.Utility;
import chess.model.pieces.King;
import chess.model.pieces.Piece;
import chess.model.pieces.Queen;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Utility class filled with structures for testing board layouts and move sequences.
 *
 * @author Arjun Srivastav
 */
public abstract class TestingSuite {
	/**
	 * Move the middle pawns forward.
	 * @return Move sequence
	 */
	public static ArrayList<String> getMiddlePawnsForwardTestingSuite() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
			"a2 a4",
			"a7 a5",
			"b2 b4",
			"b7 b5",
			"c2 c4",
			"c7 c5",
			"d2 d4",
			"d7 d5",
			"e2 e4",
			"e7 e5",
			"f2 f4",
			"f7 f5",
			"g2 g4",
			"g7 g5",
			"h2 h4",
			"h7 h5"
		));

		return res;
	}

	/**
	 * Play the dead liver opening. https://en.wikipedia.org/wiki/Two_Knights_Defense,_Fried_Liver_Attack
	 * @return Move sequence
	 */
	public static ArrayList<String> getDeadLiverOpening() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
				"e2 e4",
				"e7 e5",
				"g1 f3",
				"b8 c6",
				"f1 c4",
				"g8 f6",
				"f3 g5",
				"d7 d5",
				"e4 d5",
				"f6 d5",
				"g5 f7",
				"e8 f7"
		));

		return res;
	}

	/**
	 * Scholar's mate (short sequence that leads to checkmate)
	 * @return Move sequence
	 */

	public static ArrayList<String> getScholarsMate() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
				"e2 e4",
				"e7 e5",
				"f1 c4",
				"b8 c6",
				"d1 h5",
				"g8 f6",
				"h5 f7"
		));

		return res;
	}
	public static ArrayList<String> getEnPassant() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
				"e2 e3",
				"e7 e6",
				"f2 f3",
				"e6 e5",
				"f3 f4",
				"c7 c6",
				"f4 f5",
				"c6 c5",
				"f5 e6"
		));

		return res;
	}
	public static ArrayList<String> getPromotion() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
				"e2 e3",
				"e7 e6",
				"f2 f3",
				"e8 e7",
				"f3 f4",
				"e6 e5",
				"f4 e5",
				"e7 e6",
				"e3 e4",
				"e6 f6",
				"e5 e6",
				"f6 f5",
				"e6 e7",
				"d7 d6",
				"e7 e8"
		));

		return res;
	}
	public static ArrayList<String> getCastling() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
				"e2 e4",
				"e7 e5",
				"g1 f3",
				"b8 c6",
				"f1 b5",
				"a7 a6",
				"b5 c6",
				"d7 c6",
				"e1 g1",
				"g8 e7",
				"d2 d4",
				"e5 d4",
				"f3 d4",
				"f7 f6",
				"a2 a4",
				"e7 g6",
				"c2 c4",
				"f8 e7",
				"b2 b3",
				"e8 g8",
				"d1 d3",
				"c6 c5",
				"d4 f5",
				"d8 d3",
				"b1 d2",
				"c8 f5",
				"e4 f5",
				"g6 f4",
				"d2 f3",
				"f4 e2",
				"g1 h1",
				"e2 g3",
				"h2 g3",
				"d3 f1",
				"h1 h2",
				"e7 d6",
				"c1 b2",
				"f1 f2",
				"a1 d1",
				"d6 g3",
				"h2 h1",
				"f2 b2",
				"d1 d8",
				"f8 d8",
				"f3 e5",
				"b2 c1"
		));

		return res;
	}

	public static ArrayList<String> getStandardCheck() {
		ArrayList<String> res = new ArrayList<>(Arrays.asList(
				"e2 e4",
				"e7 e5",
				"d2 d3",
				"f7 f6",
				"d1 h5"
		));

		return res;

	}

	/**
	 * Empty board
	 * @return Board Layout
	 */
	public static Piece[][] getEmptyBoard() {
		Piece[][] board = new Piece[8][8];

		for(int i = 0; i < 8; i++)
			for(int j = 0; j < 8; j++)
				board[i][j] = null;

		return board;
	}

	/**
	 * Kings only on the board.
	 * @return Board Layout
	 */
	public static Piece[][] getKingsOnly() {
		Piece[][] board = new Piece[8][8];

		for(int i = 0; i < 8; i++)
			for(int j = 0; j < 8; j++)
				board[i][j] = null;

		board[3][4] = new King(Utility.Color.BLACK);
		board[4][2] = new King(Utility.Color.WHITE);

		return board;
	}

	/**
	 * Two kings and one queen on the board.
	 * @return Board Layout
	 */
	public static Piece[][] getKingsAndOneQueen() {
		Piece[][] board = new Piece[8][8];

		for(int i = 0; i < 8; i++)
			for(int j = 0; j < 8; j++)
				board[i][j] = null;

		board[1][6] = new King(Utility.Color.BLACK);
		board[6][1] = new King(Utility.Color.WHITE);

		board[0][0] = new Queen(Utility.Color.BLACK);
		board[7][7] = new Queen(Utility.Color.WHITE);

		return board;
	}
}
