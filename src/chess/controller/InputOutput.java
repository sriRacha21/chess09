package chess.controller;

import chess.model.Board;
import chess.model.Move;
import chess.model.Position;
import chess.model.exceptions.IllegalMoveException;
import chess.model.Utility.Color;
import chess.model.Utility.PlayState;
import chess.model.pieces.King;

import java.util.ArrayList;
import java.util.Scanner;

import static chess.model.Utility.*;

/**
 * This class handles I/O for the program, allowing the user to interface with the application.
 *
 * @author Noe Duran
 */
public class InputOutput {
	private ArrayList<String> inputTestingBuffer = new ArrayList<String>();

    /**
     * Default constructor. No arguments needed
     */
    public InputOutput() {};

    /**
     * Constructor used for debugging purposes. Input an ArrayList of moves to automatically execute them.
     * @param inputTestingBuffer Inputted moves (white,black,white,black,...) to automatically execute at the start.
     */
    public InputOutput(ArrayList<String> inputTestingBuffer) {
        this.inputTestingBuffer = inputTestingBuffer;
    }

    /**
     * The main loop of the chess program. Keeps looping as long as neither player is in checkmate or stalemate.
     * @param board The chess board to start playing on.
     */
    public void play(Board board) {
        PlayState whiteState = PlayState.NORMAL;
        PlayState blackState = PlayState.NORMAL;

        while((whiteState == PlayState.NORMAL || whiteState == PlayState.CHECK) && (blackState == PlayState.NORMAL || blackState == PlayState.CHECK)){
            whiteState = PlayState.TRY_AGAIN;
            while(whiteState == PlayState.TRY_AGAIN) {
                System.out.println(board);
                whiteState = promptMove(board, Color.WHITE, blackState == PlayState.CHECK);
            }
            if(whiteState == PlayState.DRAW || whiteState == PlayState.CHECKMATE){
              break;
            }
            blackState = PlayState.TRY_AGAIN;
            while(blackState == PlayState.TRY_AGAIN) {
                System.out.println(board);
                blackState = promptMove(board, Color.BLACK, whiteState == PlayState.CHECK);
            }
            if(blackState == PlayState.DRAW || blackState == PlayState.CHECKMATE){
                break;
            }
        }
    }

    /**
     * Helper method that prompts the user for their next move. If the move was illegal it'll ask the user to try again.
     * @param board The board that the move will be applied to.
     * @param color The color that is playing that turn.
     * @param inCheck If this color is in check.
     * @return The state of the board (checkmate, check, try again, resigned, etc.)
     */
    public PlayState promptMove(Board board, Color color, boolean inCheck) {
        Scanner getInput = new Scanner(System.in);

        System.out.print(colorToStringHR(color) + "'s Turn: ");
        String response;

        // get new input or get one from the testing buffer
        if( inputTestingBuffer.size() > 0 )
            response = inputTestingBuffer.remove(0);
        else
            response = getInput.nextLine();

        if (response.equalsIgnoreCase("resign")) {
        	System.out.println(colorToStringHR(color) + " has resigned.");
        	return PlayState.RESIGNATION;
        }
        if(response.equalsIgnoreCase("draw")){
            return PlayState.DRAW;
        }

        try {
            board.move(new Move(response, color));
        } catch (IllegalMoveException ime) {
            System.out.println("Illegal move, try again" + (DEBUG ? (": " + ime.getMessage()) : ""));
            return PlayState.TRY_AGAIN;
        }

        // check if there is a checkmate
        Color oppositeColor = getOppositeColor(color);
        King otherKing = new King(oppositeColor);
        Position kingPos = board.findPiece(otherKing);
        if( isCheckmate(otherKing, kingPos, board) ) {
            System.out.println("Checkmate\n" + colorToStringHR(color) + " wins");
            return PlayState.CHECKMATE;
        }
        // check if there is a check
        if(kingPos.isAttacked(board, oppositeColor)) {
            System.out.println("Check");
            return PlayState.CHECK;
        }

        return PlayState.NORMAL;
    }

    private boolean isCheckmate(King k, Position fromPos, Board board) {
        // if the king is not being attacked it can't be checkmate
        if(!fromPos.isAttacked(board, k.getColor()))
            return false;
        for(Position legalPosition : k.getLegalPositions(board)) {
            boolean canMoveToPos;
            try {
                canMoveToPos = k.checkMove(new Move(fromPos, legalPosition, k.getColor(), false), board);
            } catch( IllegalMoveException ime ) {
                canMoveToPos = false;
            }
            boolean canMoveToPosAndIsNotAttacked = !legalPosition.isAttackedByKing(board, k.getColor())
                    && !legalPosition.isAttacked(board, k.getColor())
                    && canMoveToPos;
            // if this space is not attacked and the king can move there, then return false
            if (canMoveToPosAndIsNotAttacked) {
                    return false;
            }
        }
        return true;
    }
}